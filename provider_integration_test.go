//go:build e2e
// +build e2e

package openstack

import (
	"crypto/rand"
	"encoding/hex"
	"io"
	"testing"
	"time"

	"gitlab.com/gitlab-org/fleeting/fleeting/integration"
	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
)

func TestProvisioning(t *testing.T) {
	name := uniqueScaleSetName()

	integration.TestProvisioning(t,
		integration.BuildPluginBinary(t, "cmd/fleeting-plugin-openstack", "fleeting-plugin-openstack"),
		integration.Config{
			PluginConfig: InstanceGroup{
				Name:              name,
				FlavorRef:         "s3.large.2",
				ImageRef:          "75937b11-ae9d-4032-8388-afaf9dd8daf6",
				NetworkUUID:       "be0bfff4-9dc3-4b8f-8db4-b79f34dc453e",
				FloatingNetworkID: "0a2228f2-7f8a-45f1-8e09-9039e1d09975",
				SecurityGroups:    []string{"allow-ssh", "default"},
			},
			ConnectorConfig: provider.ConnectorConfig{
				Username: "ubuntu",
				Timeout:  2 * time.Minute,
			},
			MaxInstances:    3,
			UseExternalAddr: true,
		},
	)
}

func uniqueScaleSetName() string {
	var buf [8]byte
	io.ReadFull(rand.Reader, buf[:])

	return "azure-fleeting-integration-" + hex.EncodeToString(buf[:])
}

// func uniquePassword() string {
// 	var buf [16]byte
// 	io.ReadFull(rand.Reader, buf[:])

// 	return "P!" + hex.EncodeToString(buf[:])
// }
