package openstack

import (
	"context"
	"crypto"
	"fmt"

	"github.com/gophercloud/gophercloud/v2/openstack/compute/v2/keypairs"
	"golang.org/x/crypto/ssh"
)

type PrivPub interface {
	crypto.PrivateKey
	Public() crypto.PublicKey
}

func (g *InstanceGroup) populateKeypair(ctx context.Context) error {
	if _, err := keypairs.Get(ctx, g.computeClient, g.Name, keypairs.GetOpts{}).Extract(); err == nil {
		err = keypairs.Delete(ctx, g.computeClient, g.Name, keypairs.DeleteOpts{}).ExtractErr()
		if err != nil {
			return err
		}
	}

	keypairsOpts := keypairs.CreateOpts{Name: g.Name}

	if g.settings.Key != nil {
		priv, err := ssh.ParseRawPrivateKey(g.settings.Key)
		if err != nil {
			return fmt.Errorf("reading private key: %w", err)
		}
		var ok bool
		key, ok := priv.(PrivPub)
		if !ok {
			return fmt.Errorf("key doesn't export PublicKey()")
		}
		sshPubKey, err := ssh.NewPublicKey(key.Public())
		if err != nil {
			return fmt.Errorf("generating ssh public key: %w", err)
		}

		keypairsOpts.PublicKey = string(ssh.MarshalAuthorizedKey(sshPubKey))
	}

	k, err := keypairs.Create(ctx, g.computeClient, keypairsOpts).Extract()
	if err != nil {
		return err
	}

	g.settings.ConnectorConfig.Key = []byte(k.PrivateKey)
	return err
}
