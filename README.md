# Fleeting Plugin Openstack

This is a [fleeting](https://gitlab.com/gitlab-org/fleeting/fleeting) plugin for AWS.

[![Pipeline Status](https://gitlab.com/cgroschupp/fleeting-plugin-openstack/badges/main/pipeline.svg)](https://gitlab.com/cgroschupp/fleeting-plugin-openstack/commits/main)

## Building the plugin

To generate the binary, ensure `$GOPATH/bin` is on your PATH, then use `go install`:

```shell
go install ./cmd/fleeting-plugin-openstack/...
```

## Plugin Configuration

The following parameters are supported:

| Parameter             | Type   | Description |
|-----------------------|--------|-------------|
| `name`                | string | Name prefix of the compute instances |
| `image_ref` | string | Image ref for the compute instances |
| `flavor_ref`    | string | flavor ref for the compute instances |
| `security_groups`    | []string | Optional. List of security groups to assign to compute instance |
| `network_uuid`    | string | Network uuid for the compute instance |
| `floating_network_id`    | string | Optional. floating_network_id for the compute instance  |
| `cloud`    | string | Optional. Openstack cloud name `OS_CLOUD` |

### Default connector config

| Parameter                | Default  |
|--------------------------|----------|
| `os`                     | `linux`  |
| `protocol`               | `ssh`    |
| `username`               | `ubuntu` |
| `use_static_credentials` | `false`  |
| `key_path`               | None. This is the path for the private key file used to connect to the runner **manager** machine. Required for Windows OS. |