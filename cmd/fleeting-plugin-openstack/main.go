package main

import (
	"flag"
	"fmt"
	"os"

	openstack "gitlab.com/cgroschupp/fleeting-plugin-openstack"
	"gitlab.com/gitlab-org/fleeting/fleeting/plugin"
)

var (
	showVersion = flag.Bool("version", false, "Show version information and exit")
)

func main() {
	flag.Parse()
	if *showVersion {
		fmt.Println(openstack.Version.Full())
		os.Exit(0)
	}

	plugin.Serve(&openstack.InstanceGroup{})
}
