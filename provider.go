package openstack

import (
	"context"
	"crypto/rand"
	"crypto/tls"
	"encoding/hex"
	"errors"
	"fmt"
	"path"
	"time"

	"github.com/gophercloud/gophercloud/v2"
	"github.com/gophercloud/gophercloud/v2/openstack"
	"github.com/gophercloud/gophercloud/v2/openstack/blockstorage/v3/volumes"
	"github.com/gophercloud/gophercloud/v2/openstack/compute/v2/keypairs"
	"github.com/gophercloud/gophercloud/v2/openstack/compute/v2/servers"
	"github.com/gophercloud/gophercloud/v2/openstack/config"
	"github.com/gophercloud/gophercloud/v2/openstack/config/clouds"
	"github.com/gophercloud/gophercloud/v2/openstack/networking/v2/extensions/layer3/floatingips"
	"github.com/gophercloud/gophercloud/v2/openstack/networking/v2/ports"
	"github.com/gophercloud/gophercloud/v2/pagination"
	"github.com/hashicorp/go-hclog"
	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
)

var _ provider.InstanceGroup = (*InstanceGroup)(nil)

type InstanceGroup struct {
	Name              string   `json:"name"`
	ImageRef          string   `json:"image_ref"`
	FlavorRef         string   `json:"flavor_ref"`
	SecurityGroups    []string `json:"security_groups"`
	NetworkUUID       string   `json:"network_uuid"`
	FloatingNetworkID string   `json:"floating_network_id"`
	ImageSize         int      `json:"image_size"`

	Cloud string `json:"cloud"`

	log      hclog.Logger
	settings provider.Settings

	computeClient *gophercloud.ServiceClient
	networkClient *gophercloud.ServiceClient
	storageClient *gophercloud.ServiceClient
}

type Duration time.Duration

func (g *InstanceGroup) Update(ctx context.Context, update func(id string, state provider.State)) error {
	err := servers.List(g.computeClient, servers.ListOpts{Name: g.Name}).EachPage(ctx, func(pCtx context.Context, p pagination.Page) (bool, error) {
		serverList, err := servers.ExtractServers(p)
		if err != nil {
			return false, err
		}
		for _, server := range serverList {
			state := provider.StateCreating
			// https://docs.openstack.org/api-guide/compute/server_concepts.html#server-status
			switch server.Status {
			case "BUILD", "MIGRATING", "PAUSED", "REBUILD":
				state = provider.StateCreating
			case "ACTIVE":
				state = provider.StateRunning
			case "DELETED", "SHUTOFF", "UNKNOWN", "ERROR":
				state = provider.StateDeleted
			default:
				g.log.Warn("unknown instance state", "instance", server.ID, "state", server.Status)
			}
			update(server.ID, state)
		}
		return true, nil
	})

	return err
}

func (g *InstanceGroup) Init(ctx context.Context, log hclog.Logger, settings provider.Settings) (provider.ProviderInfo, error) {
	g.settings = settings
	g.log = log.With("name", g.Name)

	parseOpts := []clouds.ParseOption{}
	var ao gophercloud.AuthOptions
	eo := gophercloud.EndpointOpts{}
	var tlsConfig *tls.Config
	var err error

	if g.Cloud != "" {
		parseOpts = append(parseOpts, clouds.WithCloudName(g.Cloud))
	}

	if ao, eo, tlsConfig, err = clouds.Parse(parseOpts...); err != nil {
		g.log.Info("unable to load clouds.yaml: %s, try from env", err.Error())
		ao, err = openstack.AuthOptionsFromEnv()
		if err != nil {
			return provider.ProviderInfo{}, err
		}
	}

	ao.AllowReauth = true

	providerClient, err := config.NewProviderClient(ctx, ao, config.WithTLSConfig(tlsConfig))
	if err != nil {
		return provider.ProviderInfo{}, err
	}

	computeClient, err := openstack.NewComputeV2(providerClient, eo)
	if err != nil {
		return provider.ProviderInfo{}, err
	}
	g.computeClient = computeClient

	networkClient, err := openstack.NewNetworkV2(providerClient, gophercloud.EndpointOpts{})
	if err != nil {
		return provider.ProviderInfo{}, err
	}
	g.networkClient = networkClient

	storageClient, err := openstack.NewBlockStorageV3(providerClient, gophercloud.EndpointOpts{})
	if err != nil {
		return provider.ProviderInfo{}, err
	}
	g.storageClient = storageClient

	err = g.populateKeypair(ctx)
	if err != nil {
		return provider.ProviderInfo{}, err
	}

	return provider.ProviderInfo{
		ID:        path.Join("openstack", g.Name),
		MaxSize:   1000,
		Version:   Version.String(),
		BuildInfo: Version.BuildInfo(),
	}, nil
}

func (g *InstanceGroup) getPort(ctx context.Context, serverID string) (string, error) {
	page, err := ports.List(g.networkClient, ports.ListOpts{DeviceID: serverID}).AllPages(ctx)
	if err != nil {
		return "", err
	}
	portList, err := ports.ExtractPorts(page)
	if err != nil {
		return "", err
	}
	if len(portList) != 1 {
		return "", fmt.Errorf("invalid port count")
	}
	return portList[0].ID, nil
}

func (g *InstanceGroup) getFloatingIP(ctx context.Context, portID string) (string, error) {
	page, err := floatingips.List(g.networkClient, floatingips.ListOpts{PortID: portID}).AllPages(ctx)
	if err != nil {
		return "", err
	}
	floatingIPS, err := floatingips.ExtractFloatingIPs(page)
	if err != nil {
		return "", err
	}
	if len(floatingIPS) != 1 {
		return "", fmt.Errorf("invalid floating ip count")
	}
	return floatingIPS[0].ID, nil
}

func (g *InstanceGroup) getServerFloatingIP(ctx context.Context, serverID string) (string, error) {
	if pid, err := g.getPort(ctx, serverID); err == nil {
		if fid, err := g.getFloatingIP(ctx, pid); err == nil {
			return fid, nil
		}
	}
	return "", fmt.Errorf("unable to find a floating ip for server %s", serverID)
}

func (g *InstanceGroup) getVolumeStatus(ctx context.Context, volumeID string) (string, error) {
	volume, err := volumes.Get(ctx, g.storageClient, volumeID).Extract()
	if err != nil {
		return "", err
	}

	return volume.Status, nil
}

func (g *InstanceGroup) waitForVolume(ctx context.Context, volumeID string) error {
	maxNumErrors := 10
	numErrors := 0

	for {
		status, err := g.getVolumeStatus(ctx, volumeID)
		if err != nil {
			errCode, ok := err.(*gophercloud.ErrUnexpectedResponseCode)
			if ok && (errCode.Actual == 500 || errCode.Actual == 404) {
				numErrors++
				if numErrors >= maxNumErrors {
					g.log.Error("maximum number of errors (%d) reached; failing with: %s", numErrors, err)
					return err
				}
				g.log.Error("%d error received, will ignore and retry: %s", errCode.Actual, err)
				time.Sleep(2 * time.Second)
				continue
			}

			return err
		}

		if status == "available" {
			return nil
		}

		if status == "error" {
			return errors.New("the status of volume is error")
		}

		g.log.Info("Waiting for volume creation status: %s", status)
		time.Sleep(2 * time.Second)
	}
}

func (g *InstanceGroup) Increase(ctx context.Context, delta int) (int, error) {
	for i := 0; i < delta; i++ {
		var b [8]byte
		_, err := rand.Read(b[:])
		if err != nil {
			return 0, fmt.Errorf("creating random instance name: %w", err)
		}

		networks := []servers.Network{{UUID: g.NetworkUUID}}
		opts := servers.CreateOpts{
			Name:      g.Name + "-" + hex.EncodeToString(b[:]),
			ImageRef:  g.ImageRef,
			FlavorRef: g.FlavorRef,
			Networks:  networks,
		}

		if g.ImageSize != 0 {
			volOpts := volumes.CreateOpts{
				Size:        g.ImageSize,
				ImageID:     g.ImageRef,
				Description: "Created by GitLab Fleeting Openstack Provider",
			}
			volume, err := volumes.Create(ctx, g.storageClient, volOpts, nil).Extract()
			if err != nil {
				return 0, fmt.Errorf("unable to create volume: %w", err)
			}

			err = g.waitForVolume(ctx, volume.ID)
			if err != nil {
				return 0, fmt.Errorf("unable to wait for volume: %w", err)
			}
			opts.BlockDevice = []servers.BlockDevice{{
				DeleteOnTermination: true,
				UUID:                volume.ID,
				BootIndex:           0,
				SourceType:          servers.SourceVolume,
				DestinationType:     servers.DestinationVolume,
			}}
			opts.ImageRef = ""
		}

		if g.settings.ConnectorConfig.Password != "" {
			opts.AdminPass = g.settings.ConnectorConfig.Password
		}

		if len(g.SecurityGroups) > 0 {
			opts.SecurityGroups = g.SecurityGroups
		}
		server, err := servers.Create(ctx, g.computeClient, keypairs.CreateOptsExt{CreateOptsBuilder: opts, KeyName: g.Name}, nil).Extract()
		if err != nil {
			g.log.Warn("unable to create server: %s", err.Error())
			return 0, err
		}
		if g.FloatingNetworkID != "" {
			fip, err := floatingips.Create(ctx, g.networkClient, floatingips.CreateOpts{FloatingNetworkID: g.FloatingNetworkID}).Extract()
			if err != nil {
				g.log.Warn("unable to create floating ip", err.Error())
				return 0, fmt.Errorf("creating floating ip: %w", err)
			}

			port, err := g.getPort(ctx, server.ID)
			if err != nil {
				g.log.Warn("unable to extract port: %s", err.Error())
				return 0, err
			}
			_, err = floatingips.Update(ctx, g.networkClient, fip.ID, floatingips.UpdateOpts{PortID: &port}).Extract()
			if err != nil {
				g.log.Warn("unable attach floating ip: %s", err.Error())
				return 0, err
			}
		}
	}

	return delta, nil
}

func (g *InstanceGroup) Decrease(ctx context.Context, instances []string) ([]string, error) {
	if len(instances) == 0 {
		g.log.Error("empty instances")
		return nil, nil
	}

	var succeeded []string

	for _, id := range instances {
		err := servers.Delete(ctx, g.computeClient, id).ExtractErr()
		if err != nil {
			g.log.Error("unable to delete server: %w", err)
		}
		if fid, err := g.getServerFloatingIP(ctx, id); err == nil {
			if err := floatingips.Delete(ctx, g.networkClient, fid).ExtractErr(); err != nil {
				g.log.Error("unable to delete floating ip: %w", err)
			}
		} else {
			g.log.Debug("skip deleting the floating IP from the instance because no one exists: %w", err)
		}

		succeeded = append(succeeded, id)
	}

	if len(succeeded) != len(instances) {
		g.log.Error("not all instances could be removed")
		return nil, fmt.Errorf("not all instances could be removed")
	}

	return succeeded, nil
}

func (g *InstanceGroup) ConnectInfo(ctx context.Context, id string) (provider.ConnectInfo, error) {
	info := provider.ConnectInfo{ConnectorConfig: g.settings.ConnectorConfig}

	server, err := servers.Get(ctx, g.computeClient, id).Extract()
	if err != nil {
		g.log.Error("error ConnectInfo: %w", err)
		return info, fmt.Errorf("fetching instance: %w", err)
	}

	if info.OS == "" {
		info.OS = "linux"
	}

	if info.Arch == "" {
		info.Arch = "amd64"
	}

	if info.Username == "" {
		info.Username = "ubuntu"
	}

	if info.Password == "" {
		info.Password = server.AdminPass
	}

	if info.Protocol == "" {
		info.Protocol = provider.ProtocolSSH
	}

	g.populateNetwork(&info, server)

	return info, nil
}

func (g *InstanceGroup) Shutdown(ctx context.Context) error {
	return nil
}

func (g *InstanceGroup) populateNetwork(info *provider.ConnectInfo, server *servers.Server) {
	for _, addresses := range server.Addresses {
		if networkAddresses, ok := addresses.([]interface{}); ok {
			for _, v := range networkAddresses {
				address := v.(map[string]interface{})
				if address["OS-EXT-IPS:type"] == "fixed" {
					info.InternalAddr = address["addr"].(string)
				}

				if address["OS-EXT-IPS:type"] == "floating" {
					info.ExternalAddr = address["addr"].(string)
				}
			}
		}
	}
}
